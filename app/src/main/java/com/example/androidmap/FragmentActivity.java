package com.example.androidmap;

interface FragmentActivity {
    void onRequestPermissionResult(int requestCode, String[] permissions, int[] grandresult);
}
